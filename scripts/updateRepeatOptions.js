function updateRepeatOptions(){
  var templateColumn = document.getElementById('templateColumn');
  var all = templateColumn.getElementsByClassName('row');

  var targetWords = document.getElementById('targetWords');
  var all2 = targetWords.getElementsByClassName('row');

  document.getElementById('copyResult').textContent = 'Copy Result';

  var options = [];

  for(var i = 0; i < all2.length; i++){
    var input = all2[i].querySelector('.keyword').value;
    if(input.trim() != ''){
      options.push(input.trim());
    };
  };

  var opt = '';

  for(var i = 0; i < options.length; i++){
    opt += '<a class="dropdown-item" onclick="(function (){ event.target.parentElement.parentElement.querySelector(\'button\').textContent = event.target.textContent; })()">' + options[i] + '</a>';
  };

  for(var i = 0; i < all.length; i++){
    var ddo = all[i].querySelectorAll('.col-sm-4');
    for(var j = 0; j < ddo.length; j++){
      ddo[j].querySelector('.dropdown .dropdown-menu').innerHTML = opt;
      if(typeof all[i].attributes['id'] != 'undefined' && all[i].attributes['id'].value == 'slugSufPre'){ break; };
    };
  };
};