function AddNewRepeat(){
   var templateColumn = document.getElementById('templateColumn');
   var pocket = document.getElementById('pocket');
   var all = templateColumn.getElementsByClassName('row');
   var template = all[0].outerHTML;

   template = template.replace('hidden=""', '');
   template = template.replace('style="width:0%;height:0%;overflow:hidden;visibility:hidden;"', 'style="margin-top:10px;"');

   pocket.innerHTML = template;

   all[all.length - 2].parentNode.insertBefore(pocket.getElementsByTagName('div')[0], all[all.length - 2].nextSibling);
};