function submitWork(v){
        var templateText = document.getElementById('template').value;
        var targetWords = document.getElementById('targetWords');
        var all = targetWords.getElementsByClassName('row');

        var templateColumn = document.getElementById('templateColumn');
        var allRep = templateColumn.getElementsByClassName('row');

        document.getElementById('result').value = '';

        var keywords = [];
        var keywordsClean = [];
        var repeat = [];
        var FLrepeat = [];
        var Lrepeat = [];

        for (var i = 0; i < all.length; i++) {
          var key = all[i].querySelector('div .keyword').value.trim();
          var val = all[i].querySelector('div textarea').value.trim().split('\n');
          keywords[key] = val;
        };

        for(var key in keywords){
          if(key != ''){
            keywordsClean[key] = keywords[key];
          };
        };

        keywords = keywordsClean;

        for(var i = 0; i < allRep.length; i++){
          if(typeof allRep[i].attributes['id'] != 'undefined' && allRep[i].attributes['id'].value == 'slugSufPre'){ continue; };
          var ddo = allRep[i].querySelectorAll('.col-sm-4');
          var firstNlast = [];
          for(var j = 0; j < ddo.length; j++){
            var ccc = ddo[j].querySelector('.dropdown button').textContent.trim();
            if(ccc != 'Dropdown'){
              firstNlast.push(ccc);
              if(!repeat.includes(ccc)){
                repeat.push(ccc);
              };
            };
          };

          if(firstNlast.length < 2){ firstNlast.push(firstNlast[0]); };

          if(typeof firstNlast[0] != 'undefined'){
            FLrepeat.push(firstNlast);
          };

          if(repeat.indexOf(firstNlast[0]) > repeat.indexOf(firstNlast[1])){
            [repeat[repeat.indexOf(firstNlast[0])], repeat[repeat.indexOf(firstNlast[1])]] = [repeat[repeat.indexOf(firstNlast[1])], repeat[repeat.indexOf(firstNlast[0])]];
          };
        };

        for(var key in keywordsClean){
          if(!repeat.includes(key)){
            repeat.push(key);
            Lrepeat.push(key);
          };
        };

        //console.log(repeat);
        //console.log(keywordsClean);

        if(v == 1){
          linear(templateText,keywordsClean,FLrepeat,Lrepeat);
        }else{
          var aaa = [null,null,null,null,null,null,null,null,null,null,null];
          var ixi = 0;
          for(var key in keywordsClean){
            aaa[ixi] = [];
            aaa[ixi][key] = keywords[key];
            ixi++;
          };

          var result = intoTheDeep(templateText,aaa[0],aaa[1],aaa[2],aaa[3],aaa[4],aaa[5],aaa[6],aaa[7],aaa[8],aaa[9],aaa[10]);

          for(var i = 0; i < result.length; i++){
            document.getElementById('result').value += result[i] + "\n";
          };
        };

        ///
      };