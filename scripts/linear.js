function linear(templateText,keywords,FLrepeat,Lrepeat){
        var master = [];
        var section = [];
        var nkw = [];
        var index = 0;
        var lim = 0;

        for(var i = 0; i < FLrepeat.length; i++){
          if(!master.includes(FLrepeat[i][0])){
            master.push(FLrepeat[i][0]);
          };
        };

        if(master.length > 0){
          linearRepeat(templateText,FLrepeat,master,keywords,section,nkw,index,lim);
        }else{
          linearDirect(templateText,master,keywords,section,nkw,index,lim)
        };
      };

      function linearRepeat(templateText,FLrepeat,master,keywords,section,nkw,index,lim){
        for(var key in keywords){
          if(!master.includes(key)){
            nkw[key] = keywords[key];
          };
        };

        for(var key in nkw){
          if(nkw[key].length > lim){
            lim = nkw[key].length;
          };
        };

        while(index < lim){
          section.push(templateText);
          index ++;
        };

        var pool = [];

        for(var i = 0; i < FLrepeat.length; i++){
          var aaa = [null,null,null,null,null,null,null,null,null,null,null];

          aaa[i] = [];
          aaa[i + 1] = [];
          aaa[i][FLrepeat[i][0]] = keywords[FLrepeat[i][0]];
          aaa[i + 1][FLrepeat[i][1]] = keywords[FLrepeat[i][1]];

          var result = intoTheDeep(templateText,aaa[0],aaa[1],aaa[2],aaa[3],aaa[4],aaa[5],aaa[6],aaa[7],aaa[8],aaa[9],aaa[10]);
          pool = pool.concat(result);
        };

        var reach = [];

        for(var key in keywords){
          var slug = document.getElementById('slugValue').textContent.trim();
          var prefix = document.getElementById('slugPrefix').value.trim();
          var suffix = document.getElementById('slugSuffix').value.trim();
          for(var i = 0; i < pool.length; i++){
            if(pool[i].indexOf(key) > -1){
              if(!(key in reach)){ reach[key] = 0; };
              var regex1 = new RegExp(key, "g");
              pool[i] = pool[i].replace(regex1,keywords[key][reach[key]]);

              if(key == slug){
                var spaceX = new RegExp(" ", "g");
                var ssX = (keywords[key][reach[key]].replace(spaceX,'-'));

                var regex = new RegExp("{Slug}", "g");
                pool[i] = pool[i].replace(regex,(prefix + ssX + suffix).toLowerCase());
              };
              reach[key] ++;
              if(reach[key] > (keywords[key].length - 1)){ reach[key] = 0; };
            };
          };
        };

        pool = uniq(pool);

        for(var i = 0; i < pool.length; i++){
          document.getElementById('result').value += pool[i] + "\n";
        };
      };

      function linearDirect(templateText,master,keywords,section,nkw,index,lim){
        for(var key in keywords){
          if(keywords[key].length > lim){
            lim = keywords[key].length;
          };
        };

        while(index < lim){
          section.push(templateText);

          for(var key in keywords){
            if(key == ''){
              continue;
            };

            var indexX = index;

            if(indexX > (keywords[key].length - 1)){
              indexX = (keywords[key].length - 1);
            };

            var slug = document.getElementById('slugValue').textContent.trim();
            var prefix = document.getElementById('slugPrefix').value.trim();
            var suffix = document.getElementById('slugSuffix').value.trim();

            var regex1 = new RegExp(key, "g");
            section[index] = section[index].replace(regex1, keywords[key][indexX]);

            console.log(slug);

            if(key == slug){
              var spaceX = new RegExp(" ", "g");
              var ssX = ((keywords[key][indexX]).replace(spaceX,'-'));

              var regex = new RegExp("{Slug}", "g");
              section[index] = section[index].replace(regex, (prefix + ssX + suffix).toLowerCase());
            };
          };

          index ++;
        };

        for(var i = 0; i < section.length; i++){
          document.getElementById('result').value += section[i] + "\n";
        };
      };