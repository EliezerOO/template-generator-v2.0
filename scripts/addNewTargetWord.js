function AddNewTargetWord(){
    var pocket = document.getElementById('pocket');
    var targetWords = document.getElementById('targetWords');
    var all = targetWords.getElementsByClassName('row');
    var template = all[0].outerHTML;

    template = template.replace('hidden=""', '');
    template = template.replace('style="width:0%;height:0%;overflow:hidden;visibility:hidden;"', 'style="margin-top:20px;"');

    pocket.innerHTML = template;

    all[all.length - 1].parentNode.insertBefore(pocket.getElementsByTagName('div')[0], all[all.length - 1].nextSibling);
};