function intoTheDeep(content = '',a = null,b = null,c = null,d = null,e = null,f = null,g = null,h = null,i = null,j = null,k = null){
        var answer = [];
        var slug = document.getElementById('slugValue').textContent.trim();
        var prefix = document.getElementById('slugPrefix').value.trim();
        var suffix = document.getElementById('slugSuffix').value.trim();

        if(a != null){
          for(var key1 in a){
            if(b != null){
              for(var key2 in b){
                if(c != null){
                  for(var key3 in c){
                    if(d != null){
                      for(var key4 in d){
                        if(e != null){
                          for(var key5 in e){
                            if(f != null){
                              for(var key6 in f){
                                if(g != null){
                                  for(var key7 in g){
                                    if(h != null){
                                      for(var key8 in h){
                                        if(i != null){
                                          for(var key9 in i){
                                            if(j != null){
                                              for(var key10 in j){
                                                if(k != null){
                                                  for(var key11 in k){

                                                    // core working loop START_____________________________________
                                                    for(var a1 =0; a1 < a[key1].length; a1++){
                                                      for(var a2 =0; a2 < b[key2].length; a2++){
                                                        for(var a3 =0; a3 < c[key3].length; a3++){
                                                          for(var a4 =0; a4 < d[key4].length; a4++){
                                                            for(var a5 =0; a5 < e[key5].length; a5++){
                                                              for(var a6 =0; a6 < f[key6].length; a6++){
                                                                for(var a7 =0; a7 < g[key7].length; a7++){
                                                                  for(var a8 =0; a8 < h[key8].length; a8++){
                                                                    for(var a9 =0; a9 < i[key9].length; a9++){
                                                                      for(var a10 =0; a10 < j[key10].length; a10++){
                                                                        for(var a11 =0; a11 < k[key11].length; a11++){
                                                                          answer.push(content);
                                                                          var useful = answer[answer.length - 1];

                                                                          var regex1 = new RegExp(key1, "g");
                                                                          var regex2 = new RegExp(key2, "g");
                                                                          var regex3 = new RegExp(key3, "g");
                                                                          var regex4 = new RegExp(key4, "g");
                                                                          var regex5 = new RegExp(key5, "g");
                                                                          var regex6 = new RegExp(key6, "g");
                                                                          var regex7 = new RegExp(key7, "g");
                                                                          var regex8 = new RegExp(key8, "g");
                                                                          var regex9 = new RegExp(key9, "g");
                                                                          var regex10 = new RegExp(key10, "g");
                                                                          var regex11 = new RegExp(key11, "g");

                                                                          useful = useful.replace(regex1, a[key1][a1]);
                                                                          useful = useful.replace(regex2, b[key2][a2]);
                                                                          useful = useful.replace(regex3, c[key3][a3]);
                                                                          useful = useful.replace(regex4, d[key4][a4]);
                                                                          useful = useful.replace(regex5, e[key5][a5]);
                                                                          useful = useful.replace(regex6, f[key6][a6]);
                                                                          useful = useful.replace(regex7, g[key7][a7]);
                                                                          useful = useful.replace(regex8, h[key8][a8]);
                                                                          useful = useful.replace(regex9, i[key9][a9]);
                                                                          useful = useful.replace(regex10, j[key10][a10]);
                                                                          useful = useful.replace(regex11, k[key11][a11]);

                                                                          answer[answer.length - 1] = useful;

                                                                          if(slug in a){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (a[key1][a1].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in b){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (b[key2][a2].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in c){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (c[key3][a3].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in d){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (d[key4][a4].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in e){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (e[key5][a5].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in f){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (f[key6][a6].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in g){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (g[key7][a7].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in h){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (h[key8][a8].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in i){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (i[key9][a9].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in j){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (j[key10][a10].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in k){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (k[key11][a11].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          ///////////
                                                                        };
                                                                      };
                                                                    };
                                                                  };
                                                                };
                                                              };
                                                            };
                                                          };
                                                        };
                                                      };
                                                    };
                                                    // core working loop END_____________________________________

                                                  };
                                                }else{
                                                  for(var a1 =0; a1 < a[key1].length; a1++){
                                                    for(var a2 =0; a2 < b[key2].length; a2++){
                                                      for(var a3 =0; a3 < c[key3].length; a3++){
                                                        for(var a4 =0; a4 < d[key4].length; a4++){
                                                          for(var a5 =0; a5 < e[key5].length; a5++){
                                                            for(var a6 =0; a6 < f[key6].length; a6++){
                                                              for(var a7 =0; a7 < g[key7].length; a7++){
                                                                for(var a8 =0; a8 < h[key8].length; a8++){
                                                                  for(var a9 =0; a9 < i[key9].length; a9++){
                                                                    for(var a10 =0; a10 < j[key10].length; a10++){
                                                                        answer.push(content);
                                                                        var useful = answer[answer.length - 1];

                                                                        var regex1 = new RegExp(key1, "g");
                                                                        var regex2 = new RegExp(key2, "g");
                                                                        var regex3 = new RegExp(key3, "g");
                                                                        var regex4 = new RegExp(key4, "g");
                                                                        var regex5 = new RegExp(key5, "g");
                                                                        var regex6 = new RegExp(key6, "g");
                                                                        var regex7 = new RegExp(key7, "g");
                                                                        var regex8 = new RegExp(key8, "g");
                                                                        var regex9 = new RegExp(key9, "g");
                                                                        var regex10 = new RegExp(key10, "g");

                                                                        useful = useful.replace(regex1, a[key1][a1]);
                                                                        useful = useful.replace(regex2, b[key2][a2]);
                                                                        useful = useful.replace(regex3, c[key3][a3]);
                                                                        useful = useful.replace(regex4, d[key4][a4]);
                                                                        useful = useful.replace(regex5, e[key5][a5]);
                                                                        useful = useful.replace(regex6, f[key6][a6]);
                                                                        useful = useful.replace(regex7, g[key7][a7]);
                                                                        useful = useful.replace(regex8, h[key8][a8]);
                                                                        useful = useful.replace(regex9, i[key9][a9]);
                                                                        useful = useful.replace(regex10, j[key10][a10]);

                                                                        answer[answer.length - 1] = useful;

                                                                        if(slug in a){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (a[key1][a1].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in b){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (b[key2][a2].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in c){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (c[key3][a3].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in d){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (d[key4][a4].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in e){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (e[key5][a5].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in f){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (f[key6][a6].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in g){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (g[key7][a7].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in h){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (h[key8][a8].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in i){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (i[key9][a9].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          if(slug in j){
                                                                            var spaceX = new RegExp(" ", "g");
                                                                            var ssX = (j[key10][a10].replace(spaceX,'-'));
                                                                            var regex = new RegExp("{Slug}", "g");
                                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                          };

                                                                          ///////////
                                                                    };
                                                                  };
                                                                };
                                                              };
                                                            };
                                                          };
                                                        };
                                                      };
                                                    };
                                                  };
                                                };
                                              };
                                            }else{
                                              for(var a1 =0; a1 < a[key1].length; a1++){
                                                for(var a2 =0; a2 < b[key2].length; a2++){
                                                  for(var a3 =0; a3 < c[key3].length; a3++){
                                                    for(var a4 =0; a4 < d[key4].length; a4++){
                                                      for(var a5 =0; a5 < e[key5].length; a5++){
                                                        for(var a6 =0; a6 < f[key6].length; a6++){
                                                          for(var a7 =0; a7 < g[key7].length; a7++){
                                                            for(var a8 =0; a8 < h[key8].length; a8++){
                                                              for(var a9 =0; a9 < i[key9].length; a9++){
                                                                answer.push(content);
                                                                var useful = answer[answer.length - 1];

                                                                var regex1 = new RegExp(key1, "g");
                                                                var regex2 = new RegExp(key2, "g");
                                                                var regex3 = new RegExp(key3, "g");
                                                                var regex4 = new RegExp(key4, "g");
                                                                var regex5 = new RegExp(key5, "g");
                                                                var regex6 = new RegExp(key6, "g");
                                                                var regex7 = new RegExp(key7, "g");
                                                                var regex8 = new RegExp(key8, "g");
                                                                var regex9 = new RegExp(key9, "g");

                                                                useful = useful.replace(regex1, a[key1][a1]);
                                                                useful = useful.replace(regex2, b[key2][a2]);
                                                                useful = useful.replace(regex3, c[key3][a3]);
                                                                useful = useful.replace(regex4, d[key4][a4]);
                                                                useful = useful.replace(regex5, e[key5][a5]);
                                                                useful = useful.replace(regex6, f[key6][a6]);
                                                                useful = useful.replace(regex7, g[key7][a7]);
                                                                useful = useful.replace(regex8, h[key8][a8]);
                                                                useful = useful.replace(regex9, i[key9][a9]);

                                                                answer[answer.length - 1] = useful;

                                                                if(slug in a){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (a[key1][a1].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                if(slug in b){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (b[key2][a2].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                if(slug in c){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (c[key3][a3].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                if(slug in d){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (d[key4][a4].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                if(slug in e){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (e[key5][a5].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                if(slug in f){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (f[key6][a6].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                if(slug in g){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (g[key7][a7].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                if(slug in h){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (h[key8][a8].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                if(slug in i){
                                                                  var spaceX = new RegExp(" ", "g");
                                                                  var ssX = (i[key9][a9].replace(spaceX,'-'));
                                                                  var regex = new RegExp("{Slug}", "g");
                                                                  answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                                };

                                                                          ///////////
                                                              };
                                                            };
                                                          };
                                                        };
                                                      };
                                                    };
                                                  };
                                                };
                                              };
                                            };
                                          };
                                        }else{
                                          for(var a1 =0; a1 < a[key1].length; a1++){
                                            for(var a2 =0; a2 < b[key2].length; a2++){
                                              for(var a3 =0; a3 < c[key3].length; a3++){
                                                for(var a4 =0; a4 < d[key4].length; a4++){
                                                  for(var a5 =0; a5 < e[key5].length; a5++){
                                                    for(var a6 =0; a6 < f[key6].length; a6++){
                                                      for(var a7 =0; a7 < g[key7].length; a7++){
                                                        for(var a8 =0; a8 < h[key8].length; a8++){
                                                          answer.push(content);
                                                          var useful = answer[answer.length - 1];

                                                          var regex1 = new RegExp(key1, "g");
                                                          var regex2 = new RegExp(key2, "g");
                                                          var regex3 = new RegExp(key3, "g");
                                                          var regex4 = new RegExp(key4, "g");
                                                          var regex5 = new RegExp(key5, "g");
                                                          var regex6 = new RegExp(key6, "g");
                                                          var regex7 = new RegExp(key7, "g");
                                                          var regex8 = new RegExp(key8, "g");

                                                          useful = useful.replace(regex1, a[key1][a1]);
                                                          useful = useful.replace(regex2, b[key2][a2]);
                                                          useful = useful.replace(regex3, c[key3][a3]);
                                                          useful = useful.replace(regex4, d[key4][a4]);
                                                          useful = useful.replace(regex5, e[key5][a5]);
                                                          useful = useful.replace(regex6, f[key6][a6]);
                                                          useful = useful.replace(regex7, g[key7][a7]);
                                                          useful = useful.replace(regex8, h[key8][a8]);

                                                          answer[answer.length - 1] = useful;

                                                          if(slug in a){
                                                            var spaceX = new RegExp(" ", "g");
                                                            var ssX = (a[key1][a1].replace(spaceX,'-'));
                                                            var regex = new RegExp("{Slug}", "g");
                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                          };

                                                          if(slug in b){
                                                            var spaceX = new RegExp(" ", "g");
                                                            var ssX = (b[key2][a2].replace(spaceX,'-'));
                                                            var regex = new RegExp("{Slug}", "g");
                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                          };

                                                          if(slug in c){
                                                            var spaceX = new RegExp(" ", "g");
                                                            var ssX = (c[key3][a3].replace(spaceX,'-'));
                                                            var regex = new RegExp("{Slug}", "g");
                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                          };

                                                          if(slug in d){
                                                            var spaceX = new RegExp(" ", "g");
                                                            var ssX = (d[key4][a4].replace(spaceX,'-'));
                                                            var regex = new RegExp("{Slug}", "g");
                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                          };

                                                          if(slug in e){
                                                            var spaceX = new RegExp(" ", "g");
                                                            var ssX = (e[key5][a5].replace(spaceX,'-'));
                                                            var regex = new RegExp("{Slug}", "g");
                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                          };

                                                          if(slug in f){
                                                            var spaceX = new RegExp(" ", "g");
                                                            var ssX = (f[key6][a6].replace(spaceX,'-'));
                                                            var regex = new RegExp("{Slug}", "g");
                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                          };

                                                          if(slug in g){
                                                            var spaceX = new RegExp(" ", "g");
                                                            var ssX = (g[key7][a7].replace(spaceX,'-'));
                                                            var regex = new RegExp("{Slug}", "g");
                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                          };

                                                          if(slug in h){
                                                            var spaceX = new RegExp(" ", "g");
                                                            var ssX = (h[key8][a8].replace(spaceX,'-'));
                                                            var regex = new RegExp("{Slug}", "g");
                                                            answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                          };

                                                                          ///////////
                                                        };
                                                      };
                                                    };
                                                  };
                                                };
                                              };
                                            };
                                          };
                                        };
                                      };
                                    }else{
                                      for(var a1 =0; a1 < a[key1].length; a1++){
                                        for(var a2 =0; a2 < b[key2].length; a2++){
                                          for(var a3 =0; a3 < c[key3].length; a3++){
                                            for(var a4 =0; a4 < d[key4].length; a4++){
                                              for(var a5 =0; a5 < e[key5].length; a5++){
                                                for(var a6 =0; a6 < f[key6].length; a6++){
                                                  for(var a7 =0; a7 < g[key7].length; a7++){
                                                    answer.push(content);
                                                    var useful = answer[answer.length - 1];

                                                    var regex1 = new RegExp(key1, "g");
                                                    var regex2 = new RegExp(key2, "g");
                                                    var regex3 = new RegExp(key3, "g");
                                                    var regex4 = new RegExp(key4, "g");
                                                    var regex5 = new RegExp(key5, "g");
                                                    var regex6 = new RegExp(key6, "g");
                                                    var regex7 = new RegExp(key7, "g");

                                                    useful = useful.replace(regex1, a[key1][a1]);
                                                    useful = useful.replace(regex2, b[key2][a2]);
                                                    useful = useful.replace(regex3, c[key3][a3]);
                                                    useful = useful.replace(regex4, d[key4][a4]);
                                                    useful = useful.replace(regex5, e[key5][a5]);
                                                    useful = useful.replace(regex6, f[key6][a6]);
                                                    useful = useful.replace(regex7, g[key7][a7]);

                                                    answer[answer.length - 1] = useful;

                                                    if(slug in a){
                                                      var spaceX = new RegExp(" ", "g");
                                                      var ssX = (a[key1][a1].replace(spaceX,'-'));
                                                      var regex = new RegExp("{Slug}", "g");
                                                      answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                    };

                                                    if(slug in b){
                                                      var spaceX = new RegExp(" ", "g");
                                                      var ssX = (b[key2][a2].replace(spaceX,'-'));
                                                      var regex = new RegExp("{Slug}", "g");
                                                      answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                    };

                                                    if(slug in c){
                                                      var spaceX = new RegExp(" ", "g");
                                                      var ssX = (c[key3][a3].replace(spaceX,'-'));
                                                      var regex = new RegExp("{Slug}", "g");
                                                      answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                    };

                                                    if(slug in d){
                                                      var spaceX = new RegExp(" ", "g");
                                                      var ssX = (d[key4][a4].replace(spaceX,'-'));
                                                      var regex = new RegExp("{Slug}", "g");
                                                      answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                    };

                                                    if(slug in e){
                                                      var spaceX = new RegExp(" ", "g");
                                                      var ssX = (e[key5][a5].replace(spaceX,'-'));
                                                      var regex = new RegExp("{Slug}", "g");
                                                      answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                    };

                                                    if(slug in f){
                                                      var spaceX = new RegExp(" ", "g");
                                                      var ssX = (f[key6][a6].replace(spaceX,'-'));
                                                      var regex = new RegExp("{Slug}", "g");
                                                      answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                    };

                                                    if(slug in g){
                                                      var spaceX = new RegExp(" ", "g");
                                                      var ssX = (g[key7][a7].replace(spaceX,'-'));
                                                      var regex = new RegExp("{Slug}", "g");
                                                      answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                                    };

                                                                          ///////////
                                                  };
                                                };
                                              };
                                            };
                                          };
                                        };
                                      };
                                    };
                                  };
                                }else{
                                  for(var a1 =0; a1 < a[key1].length; a1++){
                                    for(var a2 =0; a2 < b[key2].length; a2++){
                                      for(var a3 =0; a3 < c[key3].length; a3++){
                                        for(var a4 =0; a4 < d[key4].length; a4++){
                                          for(var a5 =0; a5 < e[key5].length; a5++){
                                            for(var a6 =0; a6 < f[key6].length; a6++){
                                              answer.push(content);
                                              var useful = answer[answer.length - 1];

                                              var regex1 = new RegExp(key1, "g");
                                              var regex2 = new RegExp(key2, "g");
                                              var regex3 = new RegExp(key3, "g");
                                              var regex4 = new RegExp(key4, "g");
                                              var regex5 = new RegExp(key5, "g");
                                              var regex6 = new RegExp(key6, "g");

                                              useful = useful.replace(regex1, a[key1][a1]);
                                              useful = useful.replace(regex2, b[key2][a2]);
                                              useful = useful.replace(regex3, c[key3][a3]);
                                              useful = useful.replace(regex4, d[key4][a4]);
                                              useful = useful.replace(regex5, e[key5][a5]);
                                              useful = useful.replace(regex6, f[key6][a6]);

                                              answer[answer.length - 1] = useful;

                                              if(slug in a){
                                                var spaceX = new RegExp(" ", "g");
                                                var ssX = (a[key1][a1].replace(spaceX,'-'));
                                                var regex = new RegExp("{Slug}", "g");
                                                answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                              };

                                              if(slug in b){
                                                var spaceX = new RegExp(" ", "g");
                                                var ssX = (b[key2][a2].replace(spaceX,'-'));
                                                var regex = new RegExp("{Slug}", "g");
                                                answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                              };

                                              if(slug in c){
                                                var spaceX = new RegExp(" ", "g");
                                                var ssX = (c[key3][a3].replace(spaceX,'-'));
                                                var regex = new RegExp("{Slug}", "g");
                                                answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                              };

                                              if(slug in d){
                                                var spaceX = new RegExp(" ", "g");
                                                var ssX = (d[key4][a4].replace(spaceX,'-'));
                                                var regex = new RegExp("{Slug}", "g");
                                                answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                              };

                                              if(slug in e){
                                                var spaceX = new RegExp(" ", "g");
                                                var ssX = (e[key5][a5].replace(spaceX,'-'));
                                                var regex = new RegExp("{Slug}", "g");
                                                answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                              };

                                              if(slug in f){
                                                 var spaceX = new RegExp(" ", "g");
                                                 var ssX = (f[key6][a6].replace(spaceX,'-'));
                                                 var regex = new RegExp("{Slug}", "g");
                                                 answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                              };

                                                                          ///////////
                                            };
                                          };
                                        };
                                      };
                                    };
                                  };
                                };
                              };
                            }else{
                              for(var a1 =0; a1 < a[key1].length; a1++){
                                for(var a2 =0; a2 < b[key2].length; a2++){
                                  for(var a3 =0; a3 < c[key3].length; a3++){
                                    for(var a4 =0; a4 < d[key4].length; a4++){
                                      for(var a5 =0; a5 < e[key5].length; a5++){
                                        answer.push(content);
                                        var useful = answer[answer.length - 1];

                                        var regex1 = new RegExp(key1, "g");
                                        var regex2 = new RegExp(key2, "g");
                                        var regex3 = new RegExp(key3, "g");
                                        var regex4 = new RegExp(key4, "g");
                                        var regex5 = new RegExp(key5, "g");

                                        useful = useful.replace(regex1, a[key1][a1]);
                                        useful = useful.replace(regex2, b[key2][a2]);
                                        useful = useful.replace(regex3, c[key3][a3]);
                                        useful = useful.replace(regex4, d[key4][a4]);
                                        useful = useful.replace(regex5, e[key5][a5]);

                                        answer[answer.length - 1] = useful;

                                        if(slug in a){
                                          var spaceX = new RegExp(" ", "g");
                                          var ssX = (a[key1][a1].replace(spaceX,'-'));
                                          var regex = new RegExp("{Slug}", "g");
                                          answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                        };

                                        if(slug in b){
                                          var spaceX = new RegExp(" ", "g");
                                          var ssX = (b[key2][a2].replace(spaceX,'-'));
                                          var regex = new RegExp("{Slug}", "g");
                                          answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                        };

                                        if(slug in c){
                                          var spaceX = new RegExp(" ", "g");
                                          var ssX = (c[key3][a3].replace(spaceX,'-'));
                                          var regex = new RegExp("{Slug}", "g");
                                          answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                        };

                                        if(slug in d){
                                          var spaceX = new RegExp(" ", "g");
                                          var ssX = (d[key4][a4].replace(spaceX,'-'));
                                          var regex = new RegExp("{Slug}", "g");
                                          answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                        };

                                        if(slug in e){
                                          var spaceX = new RegExp(" ", "g");
                                          var ssX = (e[key5][a5].replace(spaceX,'-'));
                                          var regex = new RegExp("{Slug}", "g");
                                          answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                        };

                                                                          ///////////
                                      };
                                    };
                                  };
                                };
                              };
                            };
                          };
                        }else{
                          for(var a1 =0; a1 < a[key1].length; a1++){
                            for(var a2 =0; a2 < b[key2].length; a2++){
                              for(var a3 =0; a3 < c[key3].length; a3++){
                                for(var a4 =0; a4 < d[key4].length; a4++){
                                  answer.push(content);
                                  var useful = answer[answer.length - 1];

                                  var regex1 = new RegExp(key1, "g");
                                  var regex2 = new RegExp(key2, "g");
                                  var regex3 = new RegExp(key3, "g");
                                  var regex4 = new RegExp(key4, "g");

                                  useful = useful.replace(regex1, a[key1][a1]);
                                  useful = useful.replace(regex2, b[key2][a2]);
                                  useful = useful.replace(regex3, c[key3][a3]);
                                  useful = useful.replace(regex4, d[key4][a4]);

                                  answer[answer.length - 1] = useful;

                                  if(slug in a){
                                    var spaceX = new RegExp(" ", "g");
                                    var ssX = (a[key1][a1].replace(spaceX,'-'));
                                    var regex = new RegExp("{Slug}", "g");
                                    answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                  };

                                  if(slug in b){
                                    var spaceX = new RegExp(" ", "g");
                                    var ssX = (b[key2][a2].replace(spaceX,'-'));
                                    var regex = new RegExp("{Slug}", "g");
                                    answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                  };

                                  if(slug in c){
                                    var spaceX = new RegExp(" ", "g");
                                    var ssX = (c[key3][a3].replace(spaceX,'-'));
                                    var regex = new RegExp("{Slug}", "g");
                                    answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                  };

                                  if(slug in d){
                                    var spaceX = new RegExp(" ", "g");
                                    var ssX = (d[key4][a4].replace(spaceX,'-'));
                                    var regex = new RegExp("{Slug}", "g");
                                    answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                                  };

                                                                          

                                                                          ///////////
                                };
                              };
                            };
                          };
                        };
                      };
                    }else{
                      for(var a1 =0; a1 < a[key1].length; a1++){
                        for(var a2 =0; a2 < b[key2].length; a2++){
                          for(var a3 =0; a3 < c[key3].length; a3++){
                            answer.push(content);
                            var useful = answer[answer.length - 1];

                            var regex1 = new RegExp(key1, "g");
                            var regex2 = new RegExp(key2, "g");
                            var regex3 = new RegExp(key3, "g");

                            useful = useful.replace(regex1, a[key1][a1]);
                            useful = useful.replace(regex2, b[key2][a2]);
                            useful = useful.replace(regex3, c[key3][a3]);

                            answer[answer.length - 1] = useful;

                            if(slug in a){
                              var spaceX = new RegExp(" ", "g");
                              var ssX = (a[key1][a1].replace(spaceX,'-'));
                              var regex = new RegExp("{Slug}", "g");
                              answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                            };

                            if(slug in b){
                              var spaceX = new RegExp(" ", "g");
                              var ssX = (b[key2][a2].replace(spaceX,'-'));
                              var regex = new RegExp("{Slug}", "g");
                              answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                            };

                            if(slug in c){
                              var spaceX = new RegExp(" ", "g");
                              var ssX = (c[key3][a3].replace(spaceX,'-'));
                              var regex = new RegExp("{Slug}", "g");
                              answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                            };

                                                                          ///////////
                          };
                         };
                        };
                    };
                  };
                }else{
                  for(var a1 =0; a1 < a[key1].length; a1++){
                     for(var a2 =0; a2 < b[key2].length; a2++){
                        answer.push(content);
                        var useful = answer[answer.length - 1];

                        var regex1 = new RegExp(key1, "g");
                        var regex2 = new RegExp(key2, "g");

                        useful = useful.replace(regex1, a[key1][a1]);
                        useful = useful.replace(regex2, b[key2][a2]);

                        answer[answer.length - 1] = useful;

                        if(slug in a){
                          var spaceX = new RegExp(" ", "g");
                          var ssX = (a[key1][a1].replace(spaceX,'-'));
                          var regex = new RegExp("{Slug}", "g");
                          answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                        };

                        if(slug in b){
                          var spaceX = new RegExp(" ", "g");
                          var ssX = (b[key2][a2].replace(spaceX,'-'));
                          var regex = new RegExp("{Slug}", "g");
                          answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                        };

                                                                          ///////////
                    };
                  };
                };
              };
            }else{
              for(var a1 =0; a1 < a[key1].length; a1++){
                  answer.push(content);
                  var useful = answer[answer.length - 1];

                  var regex1 = new RegExp(key1, "g");

                  useful = useful.replace(regex1, a[key1][a1]);

                  answer[answer.length - 1] = useful;

                  if(slug in a){
                    var spaceX = new RegExp(" ", "g");
                    var ssX = (a[key1][a1].replace(spaceX,'-'));
                    var regex = new RegExp("{Slug}", "g");
                    answer[answer.length - 1] = answer[answer.length - 1].replace(regex,(prefix + ssX + suffix).toLowerCase());
                  };

                                                                          ///////////
              };
            };
          };
        }

        return answer;
      };